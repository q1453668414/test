<?php
// example.com/src/app.php
use Symfony\Component\Routing;
use Symfony\Component\HttpFoundation\Response;
 
$routes = new Routing\RouteCollection();

// 配置路由集,一个路由就是一个可访问的地址
$routes->add('hello', new Routing\Route('/hello/{name}', array(
	'name' => 'World',
	'_controller' => function($request){
		return render_template($request);
	}
)));

$routes->add('bye', new Routing\Route('/bye',array(
	'_controller' => function($request){
		return render_template($request);
	}
)));

$routes->add('leap_year', new Routing\Route('/is_leap_year/{year}', array(
    'year' => null,
    '_controller' => function ($request) {
        if (is_leap_year($request->attributes->get('year'))) {
            return new Response('Yep, this is a leap year!');
        }
 
        return new Response('Nope, this is not a leap year.');
    }
)));

// 判断闰年功能
function is_leap_year($year = null) {
    if (null === $year) {
        $year = date('Y');
    }
 
    return 0 === $year % 400 || (0 === $year % 4 && 0 !== $year % 100);
}

// 简单的模板渲染功能
function render_template($request)
{
    extract($request->attributes->all(), EXTR_SKIP);
    ob_start();
    include sprintf(__DIR__.'/pages/%s.php', $_route);
 
    return new Response(ob_get_clean());
}

 
return $routes;